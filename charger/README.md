# 24V charger circuit
This features a linear charger circuit to charger a 24V lead acid battery.

![Circuit](./charger_24v.png)

![Simulation](./charger_24v_simulation.png)

Features:
- Powered by 230VAC
- Limited dissipation with 2-stage charging
- Battery empty LED
- Battery full LED
- Supports continuous use of rest of system connected to battery.

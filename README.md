# PokoeBox Docs
See the main project at: [timvisee/pokoebox](https://gitlab.com/timvisee/pokoebox)

## License
This project is released under the GNU GPL-3.0 license.
Check out the [LICENSE](./LICENSE) file for more information.
